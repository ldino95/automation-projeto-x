
import {Given, When, Then, And} from "cypress-cucumber-preprocessor/steps"

/* global Given, Then, When, And */
/// <reference types="Cypress"/> 



Given("que eu acesso o site", () => {
	cy.visit('http://automationpractice.com/')
    cy.validarPaginaIndex()
});

When("clico em Sign In", () => {
	cy.clicarSignIn()
});

And("informo meu email em  Create an Account", () => {
	cy.informarEmail()
});

And("clico no botao Create an Account", () => {
	cy.clicarCreateBtn()
});

Then("valido que minha conta foi criada", () => {
    cy.validarPage()
});
